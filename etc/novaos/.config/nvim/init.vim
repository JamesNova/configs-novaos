" ------------------------------------------------------------------------------
"      _ _   _
"     | | \ | |
"  _  | |  \| |  JamesNova (JN)
" | |_| | |\  |  gitlab.com/JamesNova
"  \___/|_| \_|
"
" --------------------------------- Plugins ------------------------------------
" AutoInstall Vim-Plug and Plugins
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif
call plug#begin('~/.vim/plugged')

" Coc
  Plug 'neoclide/coc.nvim', {'branch': 'release'}
  Plug 'kevinoid/vim-jsonc'
  Plug 'honza/vim-snippets'
" Treesitter
  Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
  Plug 'nvim-treesitter/nvim-treesitter-textobjects' 
" AirLine
  Plug 'vim-airline/vim-airline'
" Nerd Plugins
  Plug 'scrooloose/nerdtree'
  Plug 'Xuyuanp/nerdtree-git-plugin'
  Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
  Plug 'ryanoasis/vim-devicons'
  Plug 'preservim/nerdcommenter'
" Fuzzy Finding
  Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
  Plug 'junegunn/fzf.vim'
" Git
  Plug 'airblade/vim-gitgutter'
" Color-Schemes
  Plug 'Mofiqul/dracula.nvim'
  Plug 'morhetz/gruvbox'
  Plug 'phanviet/vim-monokai-pro'
  Plug 'shaunsingh/nord.nvim'
  Plug 'mhartington/oceanic-next'
  Plug 'drewtempelmeyer/palenight.vim'
  Plug 'overcache/NeoSolarized'

call plug#end()

" ----------------------------- General Settings -------------------------------
set number relativenumber
set backspace=indent,eol,start
set noruler
set spelllang=en_us
set confirm
set shiftwidth=2
set tabstop=2
set autoindent
set smartindent
set expandtab
set hls is
set laststatus=2
set cursorline

" -------------------------------- Key Mapping ---------------------------------
let mapleader = " "
inoremap jk <ESC>
colorscheme gruvbox
set background=dark

" NerdTree
nnoremap <leader>n :NERDTreeToggle<CR>
let g:NERDTreeGitStatusWithFlags = 1

" Fuzzy Finding
nnoremap <leader>f :GFiles<CR>

" Coc
source $HOME/.config/nvim/plug-config/coc.vim
