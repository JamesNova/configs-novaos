#!/usr/bin/env bash

cd "$HOME/.config/xmobar"

read -p "color-name: " name

read -p "bgcolor: #" bgcolor
read -p "fgcolor: #" fgcolor

read -p "color1: #" color1
read -p "color2: #" color2
read -p "color3: #" color3
read -p "color4: #" color4
read -p "color5: #" color5
read -p "color6: #" color6
read -p "color7: #" color7
read -p "color8: #" color8

config="$HOME/.config/xmobar/$name-xmobarrc"

cp .xmobarrc "$name-xmobarrc"

sed -i 's/#282c34/#'$bgcolor'/g' $config
sed -i 's/#ff6c6b/#'$fgcolor'/g' $config

sed -i 's/#51afef/#'$color1'/g' $config
sed -i 's/#ecbe7b/#'$color2'/g' $config
sed -i 's/#ff6c6b/#'$color3'/g' $config
sed -i 's/#a9a1e1/#'$color4'/g' $config
sed -i 's/#98be65/#'$color5'/g' $config
sed -i 's/#c678dd/#'$color6'/g' $config
sed -i 's/#da8548/#'$color7'/g' $config
sed -i 's/#46d9ff/#'$color8'/g' $config
